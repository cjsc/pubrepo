#!/bin/bash
#this script installs intelli-j a java app tool
LINK="https://download.jetbrains.com/idea/ideaIU-2018.2.2.tar.gz"
DOWNLOAD_PATH=/opt/PACKAGES
SOURCE_PATH=/opt/SOURCES
FILE="ideaIU-2018.2.2.tar.gz"
cd $DOWNLOAD_PATH
#verify if the package is installed if not, it downloads the package
if [ -f $FILE  ]; then
  echo "[ INFO ] IntelliJ package is already downloaded in the directory $DOWNLOAD_PATH."
else
  echo "[ INFO ] Downloading IntelliJ package..."
  cd $DOWNLOAD_PATH
  wget $LINK
fi
#unzips and runs program
 tar zxf ideaIU-2018.2.2.tar.gz -C $SOURCE_PATH
 cd $SOURCE_PATH/idea-IU-182.4129.33/bin
 echo "[ INFO ] Starting PHP storm..."
 ./idea.sh
