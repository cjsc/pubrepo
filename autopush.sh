#!/bin/bash
#this script asks if the user wants to push files
localAPI=git@bitbucket.org:cjsc/pubrepo.git
DIR="/opt/repos/pubrepo"
read -p "Do you want to upload your files on Bitbucket? (y/n):" answer
if [ $answer = y ]; then
    cd $DIR
    echo "[ INFO ] setting remote url"
    git remote set-url --add pubrepo $localAPI
    echo "[ INFO ] Pushing files in the repository..."
    git add --all
    git commit -m "Added files in the repository"
    git checkout master
    git fetch origin master
    git rebase -i origin/master
    git push origin master
else
    echo "[ INFO ] push cancelled."
fi
