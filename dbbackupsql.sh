#!/bin/bash
#dbbackupsql.sh
dumpdir="/opt/dump-backup"
sqldb_label="important_files"
now="%m-%d-%Y%H:%M:%S"
min_old="10"

#this part automatically logs in the user
  echo "Checking existing databases..."
  mysql --login-path=lgpath -e "SHOW DATABASES;"

#checks for the backup directory
  if [ $dumpdir ]; then
        echo "Backup directory already exist. $dumpdir"
  else
    mkdir -p $dumpdir
    echo "Directory created for database backup. $dumpdir"
  fi

#this part creates && compress backup for the database & dump directory
  echo "Creating Backup for $sqldb_label"
  mysqldump -u $sqldb_label > $dumpdir/$sqldb_label$(date +"$now").sql
  echo "Done backup for database $sqldb_label:"
  cd $dumpdir | ls

#this part deletes older backups of the database
  echo "Deleting backups older than $min_old minutes..."
  find $dumpdir/$sqldb_label$(date +"$now").sql -mmin +$min_old -exec rm {} \;

#this part creates backup for the dump directory on certain directories
  echo "Backing up Dump directory..."
  cd /opt
  rsync -aqz --recursive --delete --backup-dir=$dumpdir --delete $dumpdir /opt/incremental-backup
  rsync -auz --delete $dumpdir /opt/full-backup

#this part sends email notification of database backups
  echo -e "to: gilroy.toldeno@torocloud.com\nsubject: Backup Notification \n Backup for database important_files is successfull." | ssmtp gilroy.toledano@gmail.com
  echo "Backup Successfull \n email notification sent to receipient."
