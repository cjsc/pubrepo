#!/bin/bash
#this script install php tool called phpstorm
LINK="https://download.jetbrains.com/webide/PhpStorm-2018.2.2.tar.gz"
DOWNLOAD_PATH=/opt/PACKAGES
SOURCE_PATH=/opt/SOURCES
FILE="PhpStorm-2018.2.2.tar.gz"
cd $DOWNLOAD_PATH
#verify if the package is installed if not, it downloads the package
if [ -f $FILE ]; then
  echo "[ INFO ] PhpStorm package is already downloaded in the directory $DOWNLOAD_PATH."
else
  echo "[ INFO ] Downloading IntelliJ package..."
  cd $DOWNLOAD_PATH
  wget $LINK
fi
#unzips and runs program
  tar zxf PhpStorm-2018.2.2.tar.gz -C $SOURCE_PATH
  cd $SOURCE_PATH/PhpStorm-182.4129.45/bin
  echo "[ INFO ] Starting PHP Storm..."
  ./phpstorm.sh
