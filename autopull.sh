#!/bin/bash
#this script automaticall pulls files in the repository
localAPI=git@bitbucket.org:cjsc/pubrepo.git
DIR="/opt/repos/pubrepo"
    echo "[ INFO ] Pulling files in the directory"
    cd $DIR
    echo "[ INFO ] Setting remote url"
    git remote set-url --add pubrepo $localAPI
    git checkout master
    git pull --rebase origin
