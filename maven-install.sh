#!/bin/bash
LINK="http://www-us.apache.org/dist/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz"
DOWNLOAD_PATH=/opt/PACKAGES
SOURCE_PATH=/opt/SOURCES
FILE="apache-maven-3.5.4-bin.tar.gz"
cd $DOWNLOAD_PATH
#verify if the package is installed if not, it downloads the package
if [ -f $FILE  ]; then
  echo "[ INFO ] Maven package is already downloaded in the directory $DOWNLOAD_PATH."
else
  echo "[ INFO ] Downloading IntelliJ package..."
  cd $DOWNLOAD_PATH
  wget $LINK
fi
#unzips and runs program
 tar -xf apache-maven-3.5.4-bin.tar.gz -C $SOURCE_PATH
 cd $SOURCE_PATH/
 mv apache-maven-3.5.4/ apache-maven/
