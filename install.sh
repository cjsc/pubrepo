#!/bin/bash
##################################################################
#Description: Installation script of NGINX, TOMCAT, JAVA, MYSQL  #
#Script created by: Crisha Cuadro | toro.crisha.cuadro@gmail.com #
##################################################################
DL_PATH=/opt/PACKAGES
SOURCE_PATH=/opt/SOURCES
NGINX_PATH=/$SOURCE_PATH/ssl/nginx
NGINX_DL=$(https://cs.nginx.com/static/files/nginx-plus-7.4.repo)
NGINX_VER=$(nginx -v)
VER=$(rpm --query centos-release)
JAVA_VER=$(java -version)
JAVA_DL=$(--no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.tar.gz")
JAVA=$(alternatoves --config java)
TOMCAT_PATH=/$SOURCE_PATH/tomcat9
TOMCAT_DL=$(http://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.10/bin/apache-tomcat-9.0.10.tar.gz)
TOMCAT_UP=$(./bin/startup.sh)
MYSQL_VER=$(mysql --version)
MYSQL_DL=$(http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm)

#UPDATE CentOS Server
if [[ -z $($VER) ]];then
      echo "[ INFO ] Updating your OS into a newer version.."
      yum -y update
else
      $VER
      echo "[ INFO ] Your OS is up to date."
fi

#CREATE NGINX DIRECTORY
if [[ -d != $NGINX_PATH ]]; then
    echo "[ INFO ] Creating directory for NGINX"
    cd $NGINX_PATH | mkdir $NGINX_PATH
    echo "[ INFO ] Directory created for NGINX"
else
      "[ INFO ] Directory exists" $(cd $NGINX_PATH | pwd )
fi

#DOWNLOAD AND EXTRACT SOURCE CODE FROM NGINX
if [[ -z $($NGINX_VER > /dev/null ) ]]; then
      curl -o $NGINX_PATH$NGINX_DL
      yum -y install nginx-plus
      $NGINX_VER
else

#VERIFY & INSTALL JAVA VERSION
if [[ -z $($JAVA_VER 2>&1 > /dev/null) ]]; then
      echo "[ INFO ] Java is version 8."
      $JAVA_VER
else
      echo "[ ERROR ] Java is not version 8. Installing Java.."
      cd $SOURCE_PATH | wget $JAVA_DL
      tar xzf jdk-8u171-linux-x64.tar.gz
      cd $SOURCE_PATH/jdk1.8.0_171/
      echo "Choose Java version:"
      $JAVA
fi

#CREATE TOMCAT DIRECTORY
if [[ -d != $TOMCAT_PATH ]]; then
      echo "[ INFO ] Creating directory for tomcat"
      cd $SOURCE_PATH | mkdir $TOMCAT_PATH
      echo "[ INFO ] Directory created for tomcat." | pwd
else
      "[ INFO ] Directory exists" $(cd $TOMCAT_PATH | pwd )
fi

#INSTALL TOMCAT 9
if [[ != -d $TOMCAT_PATH]]; then
      echo "[ ERROR ] Tomcat not installed"
      cd $TOMCAT_PATH
      echo "[ INFO ] Installing tomcat..."
      curl -o $TOMCAT_DL
      tar xzf apache-tomcat-9.0.10.tar.gz
      mv apache-tomcat-9.0.10 tomcat9
      echo "[ INFO ] Starting tomcat.."
      cd $TOMCAT_PATH | cd $TOMCAT_UP | echo "Done."
else
    echo "[ INFO ] Tomcat is already installed."

#CHECK VER & INSTALL MYSQL
if [[ -z $($MYSQL_VER) ]]; then
      echo "[ INFO ] MySQL is installed."
else
      curl -o $MYSQL_DL | rpm -ivh mysql-community-release-el7-5.noarch.rpm
      yum update | yum -y install mysql-server
      echo "[ INFO ] Starting Mysql."
      systemctl start mysqld
      echo "[ INFO ] Mysql is running."
fi
